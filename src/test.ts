import {ActivosBuilder, DirectorActivos} from "@/app/Activos";
import {db} from "@/app/Storage";

// const f = depreciacion(40000, 20, 3)
// console.log(f)

const pk = db.newPK()
console.log(pk)

const builder = DirectorActivos.construir('01', 'Silla', 200)
    .conCustodio('Daniel')
    .conCentroCosto('Centro')
    .conEstado('defectuoso')
    .conFechaCompra(new Date("2022-10-02"))

const activo = builder.getActivo()

console.log(activo)
console.log(activo.display())