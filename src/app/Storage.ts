import Dexie, {Table} from 'dexie';
import {ActivoFijo} from "@/app/Activos";
import {Vehiculo} from "@/app/Vehiculo";

export class VehiculoData extends Vehiculo {
    tipo?: string

    calcularCostoMatricula(): number {
        return 0;
    }
}

export class DbPatrones extends Dexie {
    // 'friends' is added by dexie when declaring the stores()
    // We just tell the typing system this is the case
    activos!: Table<ActivoFijo>;
    vehiculos!: Table<VehiculoData>

    constructor() {
        super('dbPatrones');
        this.version(1).stores({
            activos: '++id, codigo, nombres', // Primary key and indexed props
            vehiculos: '++id' // Primary key and indexed props
        });
    }

    newPK(len = 5): number {
        let foo = '';
        for (let i = 0; i < len; ++i)
            foo += Math.floor(Math.random() * 10);
        return parseInt(foo)
    }

}

export const db = new DbPatrones();