export function anios() {
    const l = [];
    const anio = (new Date).getFullYear()
    for (let i = 0; i <= 20; i++) {
        l.push(anio - i)
    }
    return l
}

export function numero(s: any) {
    return parseFloat(s) ?? null
}

export function entero(s: any) {
    return parseInt(s) ?? null
}

export function formato(x: number | null): string {
    return x ? x.toFixed(2) : ""
}