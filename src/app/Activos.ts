import {depreciacion} from "@/app/Vehiculo";

/**
 * Tipo de datos que puede ser usado como atributo generico de ActivoFijo
 */
export type AtributoActivo = string | number | Date

/**
 * Clase que sera construida por el builder de mas abajo
 */
export class ActivoFijo {
    id?:number // para persistencia
    codigo: string
    nombres: string
    precio: number
    fechaCreacion?: Date

    /**
     * Contenedor de atributos dinamicos
     */
    atributos: Record<string, AtributoActivo>

    /**
     * Este objeto requiere estos tres valores por definicion
     * @param codigo
     * @param nombres
     * @param precio
     */
    constructor(codigo: string, nombres: string, precio: number) {
        this.codigo = codigo;
        this.nombres = nombres;
        this.precio = precio;
        this.atributos = {};
    }

    tiene(nombre: string): boolean {
        return nombre in this.atributos
    }

    /**
     * Utilitario, si se usa get() para tratar de sacar el valor de alguno de los valores fijos, tambien funciona
     * @param nombre
     * @protected
     */
    protected comunes(nombre: string): AtributoActivo | null {
        if (nombre === 'codigo') return this.codigo
        if (nombre === 'nombres') return this.nombres
        if (nombre === 'precio') return this.precio
        return null
    }

    get(nombre: string): AtributoActivo | null {
        const fijo = this.comunes(nombre)
        if (fijo != null) return fijo
        if (!this.tiene(nombre))
            return null
        return this.atributos[nombre]
    }

    add(nombre: string, valor: AtributoActivo): ActivoFijo {
        this.atributos[nombre] = valor
        return this;
    }

    remove(nombre: string): ActivoFijo {
        delete this.atributos[nombre]
        return this
    }

    /**
     * Utilitario para retornar el string de un atributo
     * @param nombre
     */
    getString(nombre: string): string | null {
        const val = this.get(nombre)
        return val ? val.toString() : ""
    }

    display() {
        const partes = []
        for (const k in this.atributos) {
            const val = this.getString(k)
            partes.push(`${k}: ${val}`)
        }
        return "Info: " + partes.join(", ")
    }

}

/**
 * Contrato para el builder de Activos fijos
 */
abstract class ActivosBuilderContract {

    protected activo: ActivoFijo

    constructor(activo: ActivoFijo) {
        this.activo = activo
    }

    abstract compradoHoy(): ActivosBuilder;

    abstract conFechaCompra(fecha: Date): ActivosBuilder;

    abstract conEstado(estado: string): ActivosBuilder;

    abstract conUbicacion(lugar: string): ActivosBuilder;

    abstract conCentroCosto(centro: string): ActivosBuilder;

    abstract conCustodio(nombre: string): ActivosBuilder;

    abstract sinCustodio(): ActivosBuilder;

    abstract depreciarUnAnio(): ActivosBuilder;

    abstract getActivo(): ActivoFijo;
}

/**
 * Implementacion del builder
 */
export class ActivosBuilder extends ActivosBuilderContract {

    compradoHoy(): ActivosBuilder {
        return this.conFechaCompra(new Date())
    }

    conFechaCompra(fecha: Date): ActivosBuilder {
        this.activo.add('fechaCompra', fecha)
        return this
    }

    conEstado(estado: string): ActivosBuilder {
        this.activo.add('estado', estado)
        return this
    }

    conUbicacion(lugar: string): ActivosBuilder {
        this.activo.add('ubicacion', lugar)
        return this
    }

    conCentroCosto(centro: string): ActivosBuilder {
        this.activo.add('centroCosto', centro)
        return this
    }

    conCustodio(nombre: string): ActivosBuilder {
        this.activo.add('custodio', nombre)
        return this
    }

    sinCustodio(): ActivosBuilder {
        this.activo.remove('custodio')
        return this
    }

    getActivo(): ActivoFijo {
        this.activo.fechaCreacion = new Date()
        return this.activo
    }

    depreciarUnAnio(): ActivosBuilder {
        this.activo.precio = depreciacion(this.activo.precio, 20, 1)
        return this;
    }

}

export class DirectorActivos {
    static construir(codigo: string, nombres: string, precio: number): ActivosBuilderContract {
        return new ActivosBuilder(new ActivoFijo(codigo, nombres, precio))
    }
}
