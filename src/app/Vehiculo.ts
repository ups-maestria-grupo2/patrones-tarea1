export function depreciacion(valor: number, rate: number, time: number) {
    return valor * Math.pow((1 - rate / 100), time)
}

/**
 * Clase base par el ejemplo
 */
abstract class Vehiculo {
    id?: number // para persistencia
    marca?: string;
    modelo?: string;
    anio?: number;
    avaluo?: number;

    constructor(marca?: string, modelo?: string, anio?: number, avaluo?: number) {
        this.marca = marca;
        this.modelo = modelo;
        this.anio = anio;
        this.avaluo = avaluo;
    }

    abstract calcularCostoMatricula(): number;

    // utilitarios

    protected checkArgsCalculoMatricula() {
        if (this.avaluo === null || this.anio === null)
            throw new Error("Argumentos invalidos: falta anio y avaluo")
    }

    protected devaluar(anioActual?: number) {
        if (!this.avaluo || !this.anio)
            return 0;
        if (!anioActual)
            anioActual = new Date().getFullYear();
        const diff = anioActual - this.anio;
        if (diff == 0)
            return this.avaluo
        return depreciacion(this.avaluo, 15, diff)
    }

}

class Automovil extends Vehiculo {
    calcularCostoMatricula(): number {
        this.checkArgsCalculoMatricula();
        const costoActual = this.devaluar()
        return costoActual * 0.015;
    }
}

class Camioneta extends Vehiculo {
    calcularCostoMatricula(): number {
        this.checkArgsCalculoMatricula();
        return this.devaluar() * 0.012;
    }
}

class Camion extends Vehiculo {
    calcularCostoMatricula(): number {
        this.checkArgsCalculoMatricula();
        return this.devaluar() * 0.020;
    }
}

/**
 * Clase utilitaria para manejar los tipos de vehiculos registrados
 */
export class TiposVehiculo {
    /**
     * Mapa de posibles tipos de vehiculo usado para construccion dinamica
     */
    static mapa: Record<string, any> = {
        'auto': {label: 'Automovil', clase: Automovil},
        'camioneta': {label: 'Camioneta', clase: Camioneta},
        'camion': {label: 'Camión', clase: Camion},
    }

    /**
     * Utilizado en la UI para seleccionar tipos
     */
    static tiposVista(): Array<object> {
        const res = []
        for (const tipo in TiposVehiculo.mapa) {
            const def = TiposVehiculo.mapa[tipo]
            res.push({'key': tipo, 'label': def.label})
        }
        return res
    }
}

export interface IVehiculosFactory {
    crearVehiculo(tipo: string): Vehiculo | null
}

export class VehiculoFactoryImpl implements IVehiculosFactory {

    /**
     * Instanciacion manual
     * @param tipo
     */
    crearVehiculoManual(tipo: string): Vehiculo {
        if (tipo == 'auto') return new Automovil();
        if (tipo == 'camion') return new Camion();
        if (tipo == 'camioneta') return new Camioneta();
        throw new Error(`Tipo ${tipo} no encontrado`)
    }

    /**
     * Instanciacion dinamica basada en configuracion en la clase TiposVehiculo
     * @param tipo
     */
    crearVehiculo(tipo: string): Vehiculo {
        if (!(tipo in TiposVehiculo.mapa))
            throw new Error(`Tipo ${tipo} no encontrado`)
        const def = TiposVehiculo.mapa[tipo]
        const g = new def.clase()
        return <Vehiculo>g
    }
}

export {
    Vehiculo, Automovil, Camion, Camioneta
}