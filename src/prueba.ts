class Prueba {
    name?: string
    num?: number


    constructor(name?: string, num?: number) {
        this.name = name;
        this.num = num;
    }
}

class PruebaSI extends Prueba {
    caldo?: string


    constructor(name?: string, num?: number, caldo?: string) {
        super(name, num);
        this.caldo = caldo;
    }
}

const testes = new Prueba('uno', 1)

const arr = [
    testes,
    new Prueba('dos', 2),
    new PruebaSI('tres', 3, 'caldin'),
]

var replacer = function (key: string, value: any) {
    if (value instanceof Object) {
        value['__typehint'] = value.constructor.name;
    }
    return value;
}

var reviver = function (key: string, value: any) {
    if (value instanceof Object && '__typehint' in value) {
        const name = value['__typehint']
        var obj = eval("new " + name + "();");
        for (const k in value) {
            if (k === '__typehint')
                continue
            obj[k] = value[k]
        }
        return obj
    }
    return value;
}


const str = JSON.stringify(arr, replacer);
console.log(str);

const res = JSON.parse(str, reviver)
console.log(res);






