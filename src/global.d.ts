interface JQuery {
    // eslint-disable-next-line @typescript-eslint/ban-types
    numeric(tipo?: string): JQuery;

    autonumeric(tipo?: string): JQuery;

    alpha(tipo?: string): JQuery;

    datepicker(x: object): JQuery;
}