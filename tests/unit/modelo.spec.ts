import {Camion, Vehiculo, VehiculoFactoryImpl} from "@/app/Vehiculo";

describe('Pruebas Factory Vehiculo', () => {
    it('Comprueba el tipo del factory', () => {
        const factory = new VehiculoFactoryImpl()
        const tipo = 'camion'

        const obj = factory.crearVehiculo(tipo)

        expect(obj).not.toBeNull()
        if (obj)
            expect(obj.constructor.name).toMatch(Camion.name)
        expect(obj).toBeInstanceOf(Vehiculo)

    })

    it('Comprueba que devuelva null si no hay tipo', () => {
        const factory = new VehiculoFactoryImpl()
        const esnulo = factory.crearVehiculo("nohay")
        expect(esnulo).toBeNull()
    })
})
