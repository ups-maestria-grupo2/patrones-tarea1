# Patrones de diseño de Software
## Universidad Politecnica Salesiana - Maestria de Software
### Actividad 01

Integrantes:

- Daniel Burbano
- José Castillo
- Klever Simaliza
- Carlos Párraga
- Manuel Gómez

## Descripción

Aplicación de ejemplo de patrones de diseño usando Vue.js
y Typescript con persistencia local en browser.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
